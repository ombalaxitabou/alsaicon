PATH_PREFIX = /usr/local
PATH_BIN = $(PATH_PREFIX)/bin
PATH_SHARED = $(PATH_PREFIX)/share/$(bin)
PATH_IMAGES = $(PATH_SHARED)/icons

INSTALL = /usr/bin/install
INSTALL_BIN = /usr/bin/install -m 755

# program name
bin = alsaicon
objects = flags.o alsasound.o

CC = gcc
CFLAGS = -g -Wall
LDFLAGS =

XCB = xcb-image xcb-event

# debug
#CFLAGS += -D_DEBUG

CFLAGS += `pkg-config --cflags $(XCB) alsa`
LDFLAGS += `pkg-config --libs $(XCB) alsa` -lxtrayicon -lxpmb

help: commands

## commands: show accepted commands
commands:
	grep -E '^##' Makefile | sed -e 's/## //g'

## all: compile binary
all: $(bin)

## alsaicon: compile alsaicon
$(bin): main.c $(objects)
	$(CC) $(CFLAGS) -DICON_PATH='"$(PATH_IMAGES)/%d/%s"' $(LDFLAGS) -o $@ $^

flags.c:
	$(PWD)/flags.sh ./icons

flags.h: flags.c

$(objects): %.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

## install: install alsaicon to dir set in Makefile
install: $(bin)
	$(INSTALL) -d $(PATH_BIN)/
	$(INSTALL) -d $(PATH_IMAGES)/
	$(INSTALL) -d $(PATH_IMAGES)/22/
	$(INSTALL) -d $(PATH_IMAGES)/24/
	$(INSTALL) -d $(PATH_IMAGES)/36/
	$(INSTALL) -d $(PATH_IMAGES)/48/
	$(INSTALL_BIN) -T $(bin) $(PATH_BIN)/$(bin)
	$(INSTALL) icons/22/* $(PATH_IMAGES)/22/
	$(INSTALL) icons/24/* $(PATH_IMAGES)/24/
	$(INSTALL) icons/36/* $(PATH_IMAGES)/36/
	$(INSTALL) icons/48/* $(PATH_IMAGES)/48/

## uninstall: remove installed files
uninstall:
	rm -r -f $(PATH_BIN)/$(bin) $(PATH_SHARED)/

## clean: remove compiled files (and not the installed ones)
clean:
	rm -f $(bin) $(objects) flags.c flags.h
