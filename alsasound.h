
enum retval {
	RET_SUCCESS = 0,

/* Mixer relatad error happened */
	RET_MIXER_OPEN,
	RET_MIXER_ATTACH,
	RET_MIXER_REGISTER,
	RET_MIXER_LOAD,

/* Unsufficient memory */
	RET_MEMORY,

/* Incorrect control simple element name or index */
	RET_SELEM
};

/**
 * Initialize alsa sound interface.
 * Initialize and return volume range.
 * @param soundcard name of the sound device ("default", "hw0,0")
 * @param sename name of control element ("Mster", "PCM", ...)
 * @param sindex index of control element
 * @param min return minimum sound level
 * @param max return maximum sound level
 * @param callback callback function that gets called on alsa events
 * @return 0 on success
 */
int
as_init (
		char		 *soundcard,
		char		 *sename,
		unsigned int	  sindex,
		long		 *min,
		long		 *max,
		int		(*callback)(snd_mixer_elem_t *, unsigned int));

int
as_fin ();

int
as_get_number_of_channels ();

snd_mixer_t *
as_get_mixer ();

long
as_get_volume_mono ();

long
as_get_volume_stereo ();

int
as_has_mute ();

int
as_get_mute ();

int
as_set_volume (long volume);

int
as_set_mute (int mute);

