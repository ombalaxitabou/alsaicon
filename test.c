/*
 * ALSA memory leak test
 */

#include <stdio.h>
#include <alsa/asoundlib.h>

static snd_mixer_t		*mixer;
static snd_mixer_selem_id_t	*sid = NULL;
static snd_mixer_elem_t 	*elem;

int
main ()
{
	long min, max, vol;

	/* initialize */
	snd_mixer_open (&mixer, 0);
	snd_config_update_free_global ();
	snd_mixer_attach (mixer, "default");

	snd_mixer_selem_register (mixer, NULL, NULL);
	snd_mixer_load (mixer);
	snd_mixer_selem_id_malloc (&sid);
	snd_mixer_selem_id_set_name (sid, "Master");
	snd_mixer_selem_id_set_index (sid, 0);
	elem = snd_mixer_find_selem (mixer, sid);
	snd_mixer_selem_get_playback_volume_range (elem, &min, &max);

	/* do some stuff */
	snd_mixer_selem_get_playback_volume (elem, SND_MIXER_SCHN_MONO, &vol);
	snd_mixer_selem_set_playback_volume_all (elem, 0);
	snd_mixer_selem_set_playback_volume_all (elem, vol);

	/* finalize */
	snd_mixer_close (mixer);

	if (!sid)
		return 0;

	snd_mixer_selem_id_free (sid);
	sid = NULL;

	return 0;
}
