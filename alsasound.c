#include <stdio.h>
#include <alsa/asoundlib.h>

#include "alsasound.h"

static snd_mixer_t		*mixer;
static snd_mixer_selem_id_t	*sid = NULL;
static snd_mixer_elem_t 	*elem;
static int			 nchannels;

/*
 * Finalize sound interface,
 * Close and free whatever we have opened and allocated
 */
int
as_fin ()
{
	snd_mixer_close (mixer);

	if (!sid)
		return RET_SUCCESS;

	snd_mixer_selem_id_free (sid);
	sid = NULL;

	return RET_SUCCESS;
}

/*
 * Initialize sound interface
 */
int
as_init (
		char		 *soundcard,
		char		 *sename,
		unsigned int	  sindex,
		long		 *min,
		long		 *max,
		int		(*callback)(snd_mixer_elem_t *, unsigned int))
{
	int channel;

	if (snd_mixer_open (&mixer, 0) < 0)
	{
		fprintf (stderr, "Could not open mixer\n");
		return RET_MIXER_OPEN;
	}

	if (snd_mixer_attach (mixer, soundcard) < 0)
	{
		fprintf (stderr,
			"Could not attach mixer to soundcard %s\n",
			soundcard);
		as_fin();
		return RET_MIXER_ATTACH;
	}

	if (snd_mixer_selem_register (mixer, NULL, NULL) < 0)
	{
		fprintf (stderr, "Could not register mixer simple element\n");
		as_fin();
		return RET_MIXER_REGISTER;
	}

	if (snd_mixer_load (mixer) < 0)
	{
		fprintf (stderr, "Could not load mixer\n");
		as_fin();
		return RET_MIXER_LOAD;
	}

	/* Mixer initialized ok, continue with simple element */

	if (snd_mixer_selem_id_malloc (&sid) < 0)
	{
		fprintf (stderr, "Could not allocate memory for sid\n");
		as_fin();
		return RET_MEMORY;
	}

	snd_mixer_selem_id_set_name (sid, sename);
	snd_mixer_selem_id_set_index (sid, sindex);
	elem = snd_mixer_find_selem (mixer, sid);
	if (!elem)
	{
		fprintf (stderr,
			"Could not find simple element %s,%d\n",
			sename, sindex);
		as_fin();
		return RET_SELEM;
	}

	snd_mixer_selem_get_playback_volume_range (elem, min, max);

	snd_mixer_elem_set_callback (elem, callback);

#if 0
	/*
	 * Check how human ear volume perception relate to dB values
	 */
	{
		long value;
		long dBvalue;
		double percept, pmin;

		printf ("# min %ld max %ld\n", *min, *max);
		printf ("# volume    dB  perception\n");
		for (value = *min; value <= *max; value++)
		{
			snd_mixer_selem_ask_playback_vol_dB (elem, value, &dBvalue);
			if (value == *min)
			{
				pmin = pow (2, (dBvalue / 1000.0));
			}
			percept = (pow (2, (dBvalue / 1000.0)) - pmin) / (1 - pmin);
			printf ("%6ld %6.1f %f\n", value, dBvalue / 100.0, percept);
		}
	}
#endif

	nchannels = 1;

	/* Do not count channles if it is mono */
	if (snd_mixer_selem_is_playback_mono (elem))
		return RET_SUCCESS;

	for (channel = 0; channel <= SND_MIXER_SCHN_LAST; channel++)
	{
		if (!snd_mixer_selem_has_playback_channel (elem, channel))
			nchannels++;
	}

	return RET_SUCCESS;
}

int
as_get_number_of_channels ()
{
	return nchannels;
}

snd_mixer_t *
as_get_mixer ()
{
	return mixer;
}

long
as_get_volume_mono ()
{
	long vol;

	snd_mixer_selem_get_playback_volume (elem, SND_MIXER_SCHN_MONO, &vol);

	return vol;
}

long
as_get_volume_stereo ()
{
	long left, right;

	snd_mixer_selem_get_playback_volume (
			elem, SND_MIXER_SCHN_FRONT_LEFT, &left);
	snd_mixer_selem_get_playback_volume (
			elem, SND_MIXER_SCHN_FRONT_RIGHT, &right);

	return (left + right) >> 1;
}

int
as_has_mute ()
{
	return snd_mixer_selem_has_playback_switch (elem);
}

int
as_get_mute ()
{
	if (!as_has_mute ()) return 0;

	int mute;
	snd_mixer_selem_get_playback_switch (elem, 0, &mute);
	return (mute ? 0 : 1);
}

int
as_set_volume (long volume)
{
	return snd_mixer_selem_set_playback_volume_all (elem, volume);
}

int
as_set_mute (int mute)
{
	return snd_mixer_selem_set_playback_switch_all (elem, (mute ? 0 : 1));
}
