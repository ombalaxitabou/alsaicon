#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset () */
#include <poll.h>
#include <xcb/xcb.h>
#include <xcb/xcb_event.h> /* xcb_event_get_label () */
#include <alsa/asoundlib.h> /* snd_mixer_t */
#include <xti.h>
#include <xpmb.h>

#include "flags.h"
#include "alsasound.h"

#define BUFFER_MAX 1000

/* This should be done during compile */
#ifndef ICON_PATH
#define ICON_PATH "/usr/local/share/alsaicon/icons/%d/%s"
#endif

/* Icon change boundaries */
#define ICON_LO 1 / 2
#define ICON_ME 3 / 4
#define ICON_HI 7 / 8


static char		 bg[10];
static char		 fg[10];
static unsigned int	 sec;
static char		*mixer_str = NULL;
static char		*hardware_str = NULL;
static long		 vol, min, max;
static int		 has_mute;
static int		 mute;
static x_t		*x;
static uint8_t 		 cico; /* number of currently displayed icon */

uint8_t
volume_to_icon_number ()
{
	if (mute)
		return 0;

	if (vol < (max - min) * ICON_LO)
		return 1;

	if (vol < (max - min) * ICON_ME)
		return 2;

	if (vol < (max - min) * ICON_HI)
		return 3;

	return 4;
}

/*
 * Choose best icon size for window with w width and h height
 */
int
select_icon_size (int w, int h)
{ /* suppose icons are squares */
	int i, s, m;

	if (w < h)
		m = w;
	else
		m = h;

	for (s = flag_size[0], i = 1; i < flag_size_count; i++)
	{
		if (((flag_size[i] <= m) && (flag_size[i] > s))
		|| ((s > m) && (flag_size[i] <= s)))
			s = flag_size[i];
	}

	return s;
}

unsigned char *
get_image_data (char *name, int size, int *width, int *height)
{
	unsigned char *image = NULL;
	char buffer[BUFFER_MAX];
	char *p, *b = NULL;
	char **d;
	xpm_image_t *x = NULL;
	int ret;

	if (!name || size < 0)
	{
		return NULL;
	}

	snprintf (
			buffer,
			BUFFER_MAX - 4,
			ICON_PATH,
			size,
			name);

	for (p = buffer; *p != '\0'; p++);
	strcpy (p, ".xpm");

	ret = read_file_to_buffer (buffer, &b);
	if (ret)
	{
		fprintf (stderr, "failed to load file into buffer\n");
		return NULL;
	}

	ret = parse_buffer_to_xpm_data (b, &d);
	free (b);
	if (ret)
	{
		fprintf (stderr, "failed to parse buffer to xpm data\n");
		return NULL;
	}

	ret = parse_xpm_data_to_xpm_image (d, &x);
	if (ret)
	{
		fprintf (stderr, "failed to parse xpm data\n");
		free_xpm_data (&d);
		return NULL;
	}

	*width = xpm_image_get_colums (x);
	*height = xpm_image_get_rows (x);

	if (bg[0])
	{
		ret = xpm_image_change_color_by_name (x, "background", bg);
		if (ret)
		{
			xpm_image_set_transparent_color (x, bg);
		}
	}

	if (fg[0])
	{
		xpm_image_change_color_by_name (x, "foreground", fg);
	}

	ret = parse_xpm_image_pixel_data (x, &image);
	free_xpm_image (x);
	free_xpm_data (&d);
	if (ret)
	{
		fprintf (stderr, "failed to create pixel data\n");
		return NULL;
	}

	return image;
}

int
switch_icon (x_t *x, uint8_t icon_no)
{
	int width, height, size;
	unsigned char *image = NULL;
	char fname[2];

	/* Try cache first */
	if (xti_draw_icon_from_cache (x, icon_no) == 0)
	{
		return 0;
	}

	/* This will set width and height to window's width and height */
	if (xti_get_window_size (
			xti_get_connection (x),
			xti_get_tray_icon_window (x),
			&width, &height) < 0)
	{
		/* Probably window is not exposed */
		return 1;
	}

	size = select_icon_size (width, height);

	if (icon_no > 9)
	{
		return -1;
	}

	fname[0] = (char)(48 + icon_no); /* convert int to ascii char */
	fname[1] = '\0';

	/* This will update width and height to pixmap's width and height */
	image = get_image_data (
			fname,
			size,
			&width,
			&height);
	if (!image)
	{
		/* Serious problem */
		return -1;
	}

	return xti_draw_icon_from_data (
			x,
			icon_no,
			image,
			width,
			height);
}

void
change_volume (long change)
{
	vol += change;
	as_set_volume (vol);
	cico = volume_to_icon_number ();
	switch_icon (x, cico);
}

void
toggle_mute ()
{
	if (!has_mute) return;

	mute = (mute ? 0 : 1);
	as_set_mute (mute);
	cico = volume_to_icon_number ();
	switch_icon (x, cico);
}

int
handle_alsa_events (snd_mixer_elem_t *elem, unsigned int mask)
{
/* We know that something changed, but don't know what so check everything */
	mute = as_get_mute ();
	vol  = as_get_volume_mono ();
	cico = volume_to_icon_number ();
	switch_icon (x, cico);

	return 0;
}

int
handle_xcb_events (x_t *x, xcb_generic_event_t *e, uint8_t cico)
{
	switch (e->response_type & ~0x80)
	{
	case XCB_EXPOSE:
	{
		xcb_expose_event_t *ev = (xcb_expose_event_t *)e;

		if (xti_expose_event_handler (x, cico, ev))
		{
			if (switch_icon (x, cico) < 0)
			{
				return 1;
			}
		}

		break;
	}

	case XCB_BUTTON_PRESS:
	{
		xcb_button_press_event_t *ev = (xcb_button_press_event_t *)e;

		switch (ev->detail)
		{
		case 1:
			/* Toggle mute on left click */
			toggle_mute ();
			break;

		case 3:
			/* Exit on right mouse click */
			return 1;

		case 4:
			/* Mouse wheel scroll up, increase volume level */
			change_volume (+1);
			break;

		case 5:
			/* Mouse wheel scroll down, decrease volume level */
			change_volume (-1);
			break;

		default:
			break;

		}

		break;
	}

	case XCB_GRAPHICS_EXPOSURE:
	case XCB_NO_EXPOSURE:
	case XCB_REPARENT_NOTIFY:
	case XCB_MAP_NOTIFY:
		break;

	case XCB_UNMAP_NOTIFY:
	{
		if (!xti_request_dock_timeout (
				xti_get_connection (x),
				xti_get_tray_icon_window (x),
				xti_get_screen_number (x),
				sec))
		{
			return 1;
		}

		xti_draw_icon_from_cache (x, cico);

		break;
	}

	default:
	{
		/* Unknown event type, ignore it */
		if (xcb_event_get_label (e->response_type))
		{
			fprintf (
					stderr,
					"Unknown event: %s\n",
					xcb_event_get_label (e->response_type));
		}
		break;
	}

	} /* switch */

	return 0;
}

int
setup_fds (x_t *x, struct pollfd **curfds, int *nfds, snd_mixer_t *mixer)
{
	struct pollfd	*fds = *curfds;
	int		 n;

	/* Count file descriptors */
	n = snd_mixer_poll_descriptors_count (mixer);
	n++; /* XCB file descriptor */
	if (n != *nfds)
	{
		*nfds = n;
		fds = realloc (*curfds, n * sizeof (struct pollfd));
		if (!fds)
		{
			if (*curfds) free (*curfds);
			fprintf (stderr, "Not enough memory to allocate pollfds\n");
			return 1;
		}
		*curfds = fds;
	}

	/* ALSA */
	if (snd_mixer_poll_descriptors (mixer, fds, n) < 0)
	{
		fprintf (stderr, "Couldt get ALSA poll descriptors\n");
		return 2;
	}

	/* XCB */
	fds[n - 1].fd = xcb_get_file_descriptor (xti_get_connection (x));
	fds[n - 1].events = POLLIN;

	return 0;
}

int
loop ()
{
	struct pollfd		*fds  = NULL;
	int			 nfds = 0;
	int			 i;
	int			 exit = 0;
	short unsigned int	 revents;
	xcb_generic_event_t	*e;
	snd_mixer_t		*mixer = as_get_mixer ();

	while (!exit)
	{
		setup_fds (x, &fds, &nfds, mixer);

		i = poll (fds, nfds, -1); /* errno */
		if (i < 0)
		{
			if (fds) free (fds);
			fprintf (stderr, "Poll error\n");
			return 1;
		}

		/* get ALSA events */
		if (snd_mixer_poll_descriptors_revents (
			mixer, fds, nfds - 1, &revents) < 0)
		{
			if (fds) free (fds);
			fprintf (stderr, "Error processing ALSA poll events\n");
			return 2;
		}

		/* handle ALSA events */
		if (revents & (POLLERR | POLLNVAL))
		{
			if (fds) free (fds);
			fprintf (stderr, "Error processing ALSA poll events\n");
			return 3;
		}
		else if (revents & POLLIN)
		{
			snd_mixer_handle_events (mixer);
		}

		/* handle XCB events */
		if (fds[nfds - 1].revents & POLLIN)
		{
			e = xcb_poll_for_event (xti_get_connection (x));
			while (e)
			{
				exit += handle_xcb_events (x, e, cico);
				free (e);
				e = xcb_poll_for_event (xti_get_connection (x));
			}
			exit += xcb_connection_has_error (xti_get_connection (x));
		}
	}

	if (fds) free (fds);

	return 0;
}

int
main (int argc, char **argv)
{
	int		 i, l, ret;

	memset (bg, 0, 10);
	memset (fg, 0, 10);

	/* Parse command line arguments */
	for (i = 1; i < argc - 1; i++)
	{
		if (argv[i][0] != '-') continue;

		switch (argv[i][1])
		{
		case 'b':
			l = strlen (argv[i + 1]);
			if (l != 7 && l != 9) break;

			strncpy (bg, argv[i + 1], l);
			bg[l + 1] = '\0';
			i++;
			break;

		case 'f':
			l = strlen (argv[i + 1]);
			if (l != 7 && l != 9) break;

			strncpy (fg, argv[i + 1], l);
			fg[l + 1] = '\0';
			i++;
			break;

		case 'w':
			if (sscanf (argv[i + 1], "%u", &sec) == 1)
			{
				i++;
			}
			else
			{
				sec = 0;
			}
			break;

		case 'm':
			if (!mixer_str)
			{
				mixer_str = strdup (argv[i + 1]);
				i++;
			}
			break;

		case 'd':
			if (!hardware_str)
			{
				hardware_str = strdup (argv[i + 1]);
				i++;
			}
			break;

		default:
			break;
		}
	}

	/* Initialize system tray icon */
	x = xti_initialize_tray_icon (NULL, sec, "alsaicon");
	if (!x)
	{
		return 1;
	}

	/* Initialize sound */
	if (!mixer_str)
	{
		mixer_str = strdup ("Master");
	}
	if (!hardware_str)
	{
		hardware_str = strdup ("default");
	}
	ret = as_init (hardware_str, mixer_str, 0, &min, &max, handle_alsa_events);
	free (mixer_str);
	mixer_str = NULL;
	free (hardware_str);
	hardware_str = NULL;
	if (ret != RET_SUCCESS)
	{
		xti_free_and_close_tray_icon (x);
		return 1;
	}
	has_mute = as_has_mute ();
	mute = as_get_mute ();
	vol  = as_get_volume_mono ();

	/* Load and display first icon */
	cico = volume_to_icon_number ();
	switch_icon (x, cico);

	ret = loop ();

	/* Clean up */
	as_fin ();
	xti_free_and_close_tray_icon (x);

	return ret;
}
