#!/bin/dash

# use dash for develpment to make sure no bashisms get in this script
# use /bin/sh later

flagdir=$1

# find out which icon sizes are available
sizes=`dir -1 "$flagdir" | grep [0-9].`

flag_exists ()
{
	# check if flag's image file exists in every directory
	for s in $sizes
	do
		# do not check other sizes if one is missing
		if test ! -f "$flagdir"/$s/$1
		then
			echo 0
			return
		fi
	done

	echo 1
}

# find the first directory in "$flagdir"
# there shouldn't be any other directories than the ones containing flag pixmaps
for d in $sizes
do
	if test -d "$flagdir"/$d
	then
		break
	fi
done

# flag names should be xx.xpm
candidates=`dir -1 "$flagdir"/$d`
flags=''
temp=''
count=0

# print file header
echo "
/*
 * This file is an automatically generated file listing x11 pixmaps of
 * country flags. These should be available in all sizes.
 */
" > ./flags.c

echo "
/*
 * This file is an automatically generated file listing x11 pixmaps of
 * country flags. These should be available in all sizes.
 */
" > ./flags.h

for c in $candidates
do
	if test 1 -eq $(flag_exists $c)
	then
		if test -z "$flags"
		then
			flags=`echo \"$c\"`
		else
			flags=`echo $flags, \"$c\"`
		fi
		count=$(($count+1))
	fi
done

# print flag file name array
echo "const char flag_name[$count][12] = {" >> ./flags.c
for f in $flags
do
	echo "	$f" >> ./flags.c
done
echo "};

const int flag_count = $count;
" >> ./flags.c

echo "
extern const char flag_name[$count][12];
extern const int flag_count;
" >> ./flags.h

# print flag size array
temp=''
# reset counter to count available sizes
count=0
for s in $sizes
do
	# spaces are not allowed in directory names
	if test ! -d "$flagdir"/$s
	then
		continue
	fi

	if test -z "$temp"
	then
		temp=`echo $s`
	else
		temp=`echo $temp, $s`
	fi

	count=$(($count+1))
done
echo "const int flag_size[$count] = { $temp };

const int flag_size_count = $count;
" >> ./flags.c

echo "extern const int flag_size[$count];
extern const int flag_size_count;
" >> ./flags.h
